import initModels from "../models/init-models.js";
import sequelize from '../models/index.js'
import { errorCode, successCode } from "../config/response.js";

// * thao tác thông qua initModels 
const models = initModels(sequelize) ; 


const addLike = async (req , res) => {
    try {
        let {user_id , res_id , date_like} = req.body ; 
        let dataResLike = {user_id , res_id , date_like} ;
        let data = await models.like_res.create(dataResLike) ;
        successCode(res , data , 'Like thành công') ;
    } catch (error) {
        errorCode(res , 'Lỗi hệ thống') ; 
    }
}

const unLike = async (req, res) => {
    try {
        let {user_id , res_id} = req.body ;
        let data = await models.like_res.destroy({
            where : {
                user_id : user_id ,
                res_id : res_id
            }
        })
        successCode(res , "Unlike thành công") ; 
        
    } catch (error) {
        errorCode(res , 'Lỗi hệ thống') ; 
    }
}


const getListLikeByRestaurant = async(req , res) => {
    try {
        const id = req.params.id ; 
        let data = await models.restaurant.findAll({
            where : {
                res_id : id
            } , 
            include : ["user_id_users"]
        })
        successCode(res , data , "Lấy thành công") ; 
    } catch (error) {
        errorCode(res , "Lỗi hệ thống") ; 
    }
}

const getListLikeByUser = async (req , res) => {
    try {
        const id = req.params.id ; 
        let data = await models.user.findAll({
            where : {
                user_id : id
            } , 
            include : ['res_id_restaurants']
        })
        successCode(res , data , "Lấy thành công") ; 
    } catch (error) {
        errorCode(res , "Lỗi hệ thống") ; 
    }
}

export {
    getListLikeByRestaurant , 
    getListLikeByUser , 
    addLike , 
    unLike
}