import { errorCode, successCode } from "../config/response.js";
import sequelize from "../models/index.js";
import initModels from "../models/init-models.js";


const models = initModels(sequelize) ; 

const addOrder = async (req , res) => {
    try {
        let {user_id , food_id , amount , arr_sub_id , code} = req.body ; 
        let dataOrder = {user_id , food_id , amount , arr_sub_id , code} ;
        let data = await models.order.create(dataOrder) ; 
        successCode(res , data , "Thêm order thành công")
    } catch (error) {
        errorCode(res , "Lỗi server")
        
    }
}

export {
    addOrder
}