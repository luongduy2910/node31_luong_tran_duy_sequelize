import initModels from "../models/init-models.js";
import sequelize from '../models/index.js'
import { errorCode, successCode } from "../config/response.js";

const models = initModels(sequelize) ; 

const addRate = async (req, res) => {
    try {
        let {user_id , res_id , amount , date_rate} = req.body ; 
        let dataRate = {user_id, res_id , amount, date_rate} ; 
        let data = await models.rate_res.create(dataRate) ; 
        successCode(res , data , "Thêm đánh giá thành công") ;
        
    } catch (error) {
        errorCode(res , "Lỗi hệ thống") ; 
    }
}

const getRateByRestaurant = async (req, res) => {
    try {
        let id = req.params.id ;
        let data = await models.restaurant.findAll({
            where : {
                res_id : id
            } , 
            include : ["user_id_user_rate_res"] 
        })
        successCode(res , data , "Lấy thành công") ; 
    } catch (error) {
        errorCode(res , "Lỗi hệ thống") ;
    }
    
}

const getRateByUser = async (req, res) => {
    try {
        let id = req.params.id ;
        let data = await models.user.findAll({
            where : {
                user_id : id
            } , 
            include : ["res_id_restaurant_rate_res"] 
        })
        successCode(res , data , "Lấy thành công") ; 
    } catch (error) {
        errorCode(res , "Lỗi hệ thống") ; 
    }
    
}

export {
    addRate , 
    getRateByRestaurant , 
    getRateByUser
}