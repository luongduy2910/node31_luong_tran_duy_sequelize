import express from "express";
import { getListLikeByRestaurant , getListLikeByUser , addLike , unLike } from "../controllers/likeController.js";

const likeRouter = express.Router() ; 

likeRouter.post('/add-like' , addLike) ; 
likeRouter.delete('/unlike' , unLike) ; 
likeRouter.get('/getLike-restaurants/:id' , getListLikeByRestaurant) ;
likeRouter.get('/getLike-users/:id' , getListLikeByUser) ; 

export default likeRouter ; 