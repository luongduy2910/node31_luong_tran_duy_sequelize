import express from "express";
import { addRate , getRateByRestaurant , getRateByUser} from "../controllers/rateController.js";

const rateRouter = express.Router() ; 

rateRouter.post('/add-rate' , addRate) ; 
rateRouter.get('/getRate-restaurants/:id' , getRateByRestaurant) ; 
rateRouter.get('/getRate-users/:id' , getRateByUser) ; 

export default rateRouter ; 